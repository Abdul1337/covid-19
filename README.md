## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

## Disclaimer

This is not an official government project. This is not associated in any way with my employer. I am not getting paid to do this. Also, I have no intentions of making money from this. This is created to keep people informed about the current state of Covid-19 in the country. If you need help, please call the emergency numbers listed on the [website here](https://www.mohfw.gov.in/) or call 100.

If you feel any information is missing or there is any error, reach out to me directly on [Twitter](https://twitter.com/snapthatnow) and I would be happy to assist.

## Credits

This would not have been possible without the awesome guys at https://github.com/covid19india. This project internally calls their API. They are doing really awesome work maintaining a crowdsorced data-base of patients. Thanks to them for all the hard work!