import React from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

const Footer = props => {
  const {t} = useTranslation();
  return (
    <footer className="footer">
      <span>
        <Link to="/help">{t('Help')}</Link>
      </span>
      |
      <span>
        <a
          href="http://twitter.com/snapthatnow"
          target="_blank"
          rel="noopener noreferrer"
        >
          {t('Created by Abdul1337')}
        </a>
      </span>
    </footer>
  );
};

export default React.memo(Footer);
